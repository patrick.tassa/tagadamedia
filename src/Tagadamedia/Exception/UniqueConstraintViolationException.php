<?php

namespace Tagadamedia\Exception;


final class UniqueConstraintViolationException extends \Exception
{
}