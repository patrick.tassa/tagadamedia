<?php

namespace Tagadamedia\Exception;


final class MemberAlreadyInTeamException extends \Exception
{
}