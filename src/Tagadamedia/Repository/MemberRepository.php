<?php

namespace Tagadamedia\Repository;

use Tagadamedia\Entity\EntityInterface;

/***
 * Class MemberRepository
 * @package Tagadamedia\Repository
 */
class MemberRepository implements RepositoryInterface
{

    /**
     * @param mixed $id
     * @return mixed
     */
    public function find($id)
    {
        throw new \Exception('TODO: Implement find() method.');
    }

    /**
     * @param EntityInterface $object
     * @return mixed
     */
    public function persist(EntityInterface $object)
    {
        throw new \Exception('TODO: Implement persist() method.');
    }
}
