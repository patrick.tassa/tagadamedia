<?php

namespace Tagadamedia\Repository;

use Tagadamedia\Entity\EntityInterface;

/***
 * Interface RepositoryInterface
 * @package Tagadamedia\Repository
 */
interface RepositoryInterface
{

    /**
     * Cherche un objet par son identifiant
     *
     *
     * @param mixed  $id        Identifiant de l'objet
     *
     * @return object L'objet trouvé.
     */
    public function find($id);

    /**
     * Persiste un objet
     *

     * @param EntityInterface $object L'instance de l'objet à persister
     *
     * @return void
     */
    public function persist(EntityInterface $object);
}
