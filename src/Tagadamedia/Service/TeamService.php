<?php

namespace Tagadamedia\Service;


use Tagadamedia\Entity\Team;
use Tagadamedia\Exception\MemberAlreadyInTeamException;
use Tagadamedia\Repository\MemberRepository;
use Tagadamedia\Repository\TeamRepository;

class TeamService
{
    /**
     * @var TeamRepository
     */
    private $repository;

    /**
     * @var MemberRepository
     */
    private $memberRepository;

    /***
     * TeamService constructor.
     * @param TeamRepository $repository
     * @param MemberRepository $memberRepository
     */
    public function __construct(TeamRepository $repository, MemberRepository $memberRepository)
    {
        $this->repository = $repository;
        $this->memberRepository = $memberRepository;
    }

    /***
     * Permet d'ajouter un membre à une équipe
     *
     * @param $id           Identifiant de l'équipe
     * @param $memberId     Identifiant du membre
     */
    public function add($id, $memberId)
    {
        /* @var Team $team */
        $team = $this->repository->find($id);
        $members = $team->getMembers();

        if (count($members) >= Team::MEMBERS_LIMIT) {
            throw new TooManyMembersException("Nombre de membres maximum déjà atteint");
        }

        $addMember = $this->memberRepository->find($memberId);
        if ($team->isMemberInTeam($addMember)) {
            throw new MemberAlreadyInTeamException("Le membre est déjà présent dans l'équipe");
        }

        // Ajout du membre à l'équipe
        $team->addMember($addMember);
        $this->repository->persist($team);
    }
}
