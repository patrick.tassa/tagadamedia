<?php

namespace Tagadamedia\Entity;

/***
 * Class Member
 * @package Tagadamedia\Entity
 */
final class Member implements EntityInterface
{
    /**
     * @var integer
     */
    private $id;

    /***
     * Member constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }


    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
