<?php

namespace Tagadamedia\Entity;

/***
 * Class Team
 * @package Tagadamedia\Entity
 */
final class Team implements EntityInterface
{
    /**
     * Nombre de membres maximum par équipe
     */
    const MEMBERS_LIMIT = 3;

    /**
     * @var integer
     */
    private $id;

    /**
     * @var Member[]
     */
    private $members = array();

    /***
     * Team constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Member[]
     */
    public function getMembers()
    {
        return $this->members;
    }

    /**
     * Ajout d'un membre à l'équipe
     * @param Member $member
     */
    public function addMember(Member $member)
    {
        $this->members[] = $member;
    }

    /**
     * Vérifie si le membre est déjà dans l'équipe
     *
     * @param Member $member
     * @return bool
     */
    public function isMemberInTeam(Member $member)
    {
        foreach ($this->members as $memberT) {
            if ($memberT->getId() == $member->getId()) {
                return true;
            }
        }

        return false;
    }
}
