<?php

namespace Tests\Tagadamedia\Mock;

use Tagadamedia\Entity\EntityInterface;
use Tagadamedia\Entity\Member;
use Tagadamedia\Exception\UniqueConstraintViolationException;
use Tagadamedia\Repository\MemberRepository;

class MockMemberRepository extends MemberRepository
{

    protected $records = [
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4
    ];

    /**
     * @param int $id
     * @return Member
     */
    public function find($id)
    {
        if (isset($this->records[$id])) {
            return new Member(1);
        } else {
            throw new \OutOfBoundsException(sprintf('member %d does not exist', $id));
        }
    }

    /***
     * @param EntityInterface $member
     */
    public function persist(EntityInterface $member)
    {
        $this->records[$member->getId()] = $member;
    }
}
