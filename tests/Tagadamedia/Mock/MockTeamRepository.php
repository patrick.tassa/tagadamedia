<?php

namespace Tests\Tagadamedia\Mock;

use Tagadamedia\Entity\EntityInterface;
use Tagadamedia\Entity\Team;
use Tagadamedia\Exception\UniqueConstraintViolationException;
use Tagadamedia\Repository\TeamRepository;

class MockTeamRepository extends TeamRepository
{
    protected $items = [
        1 => 1
    ];

    /**
     * @param int $id
     * @return Team
     */
    public function find($id)
    {
        if (isset($this->items[$id])) {
            return new Team($id);
        } else {
            throw new \OutOfBoundsException(sprintf('team %d does not exist', $id));
        }
    }

    /***
     * @param EntityInterface $team
     * @throws UniqueConstraintViolationException
     */
    public function persist(EntityInterface $team)
    {
        $this->items[$team->getId()] = $team;
    }
}