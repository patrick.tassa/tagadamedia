<?php

namespace Tests\Tagadamedia\Entity;

use PHPUnit\Framework\TestCase;
use Tagadamedia\Entity\Member;
use Tagadamedia\Entity\Team;


class TeamTest extends TestCase
{
    /**
     * @var Team
     */
    protected $team;

    public function setUp()
    {
        $this->team = new Team(12);
    }

    /**
     * Teste qu'une nouvelle équipe n'a pas de membre
     *
     * @test
     */
    public function newTeamHasNoMembers()
    {
        self::assertEmpty($this->team->getMembers());
    }

    /**
     * Teste si le membre est déjà dans l'équipe
     *
     * @test
     */
    public function isMemberInTeam()
    {
        $member = new Member(1);

        $this->team->addMember($member);

        self::assertEquals(true, $this->team->isMemberInTeam($member));
    }

    /**
     * Teste si le membre n'est pas dans l'équipe
     *
     * @test
     */
    public function isMemberNotInTeam()
    {
        self::assertNotEquals(true, $this->team->isMemberInTeam(new Member(1)));
    }

}