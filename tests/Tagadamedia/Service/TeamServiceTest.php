<?php

namespace Tests\Tagadamedia\Service;

use PHPUnit\Framework\TestCase;
use Tagadamedia\Service\TeamService;
use Tests\Tagadamedia\Mock\MockMemberRepository;
use Tests\Tagadamedia\Mock\MockTeamRepository;


class TeamServiceTest extends TestCase
{
    /**
     * @var TeamService
     */
    protected $teamService;

    public function setUp()
    {
        $this->teamService = new TeamService(new MockTeamRepository(), new MockMemberRepository());
    }

    /**
     * Teste l'ajout d'un membre à une équipe inexistante
     *
     * @test
     */
    public function addMemberNonexistentTeam()
    {
        self::expectException('OutOfBoundsException');

        $this->teamService->add(100, 1);
    }

    /**
     * Teste l'ajout d'un membre inexistant à une équipe
     *
     * @test
     */
    public function addNonexistentMemberInTeam()
    {
        self::expectException('OutOfBoundsException');

        $this->teamService->add(1, 100);
    }

    /**
     * Teste l'ajout d'un membre à une équipe
     *
     * @test
     */
    public function add()
    {
        $this->teamService->add(1, 1);

        $this->assertTrue(TRUE);
    }
}