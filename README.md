Tagadamedia technical test
==========================

Ce projet a pour but de tester les compétences techniques des candidats.

## Installation

La manière la plus simple d'installer le projet est d'utiliser composer

```
curl -sS https://getcomposer.org/installer | php
```

Ensuite, on installe les dépendances

```
php composer.phar install
```

et on peut lancer les tests unitaires

```
bin/phpunit
```

## Exercice 1

Il s'agit de remanier le code pour corriger les erreurs éventuelles et rendre le code de meilleure qualité possible (améliorer la lisibilité, la maintenabilité) pour qu'il soit exploitable par toute l'équipe.

## Exercice 2 (optionnel)

Il faut ajouter autant de tests unitaires que nécessaire pour garantir que les régressions seront détectées dès leur apparition